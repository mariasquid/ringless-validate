<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Run extends Model
{

    protected $table        = 'run';
    protected $primaryKey   = 'campaignid';
    protected $fillable     = ['name_campaign', 'trunkid', 'trunk_name', 'connection_string', 'sleep_time', 'timer_enabled', 'status', 'pause', 'start_time', 'stop_time', 'available_numbers', 'total_numbers', 'prefix_dial', 'pid_number', 't1', 't2', 't3'  ];

     /**
     * Get the country for the Run.
     */
    public function country()
    {
        return $this->belongsTo('App\Country', 'country_code');
    }

    /**
     * Get the trunk for the Run.
     */
    public function trunk()
    {
        return $this->belongsTo('App\Trunk', 'trunkid');
    }

    /**
     * Get the campaign for the Run.
     */
    public function campaign()
    {
        return $this->belongsTo('App\Campaign', 'campaignid');
    }
}
