<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $primaryKey = 'country_code';
    public $incrementing = false;
    protected $table = 'country';
    protected $fillable = ['country_code', 'name'];

    /**
     * Get the Numbers for the Country.
     */
    public function run()
    {
        return $this->hasMany('App\Run', 'country_code', 'country_code');
    }
}
