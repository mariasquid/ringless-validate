<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    //
    protected $table = 'campaign';
    protected $fillable = [ 'name', 'description'];

    /**
     * Get the number for the Campaign.
     */
    public function numbers()
    {
        return $this->hasMany('App\Numbers', 'campaignid', 'id');
    }

    /**
     * Get the Run for the Campaign.
     */
    public function run()
    {
        return $this->hasOne('App\Run', 'campaignid', 'id');
    }

}
