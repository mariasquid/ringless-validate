<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Numbers extends Model
{
    protected $table        = 'numbers';

    protected $primaryKey   = 'phonenumber';
    
    protected $fillable     = ['phonenumber', 'status', 'campaignid', 'country_code', 'amd_status'];

    /**
     * Get the country for the Number.
     */
    public function country()
    {
        return $this->belongsTo('App\Country', 'country_code');
    }

    /**
     * Get the campaign for the Number.
     */
    public function campaign()
    {
        return $this->belongsTo('App\Campaign', 'campaignid');
    }

}
