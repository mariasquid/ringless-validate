<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trunk extends Model
{
    //
    protected $table = 'trunk';
    protected $fillable = [
    	'id',
    	'name',
    	'ip_address',
    	'connection_string'
    ];
}
