<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Exception\ProcessTimedOutException;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('test');
    }

    public function call(Request $request)
    {
        $number = $request->input('phone_number');
        $callerId1 = $request->input('caller_id1');
        $callerId2 = $request->input('caller_id2');
        $trunk = $request->input('trunk');
    //dd($request->all());
        $process = new Process("php caller/ringless-test.php $number $callerId1 $callerId2 $trunk");
        $process->run();
        
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        
        //echo $process->getOutput();
     //   dd("LLegoo aaca!");
        return view('status');
    }

    public function status()
    {
        return view('status');
    }

    public function execute_status()
    {
//        dd("in ajax controller");
	set_time_limit(120);

        header("Connection: Keep-alive");
        
/*        $servidor = stream_socket_server("tcp://127.0.0.1:1339", $errno, $errorMessage);
        if ($servidor === false) {
            throw new UnexpectedValueException("No se ha podido enlazar el socket: $errorMessage");
        }
        for (;;) {
            $cliente = @stream_socket_accept($servidor);
dd($cliente);

            $message= fread($cliente, 1024);
            echo $message."<br>";
            fputs ($cliente, "OK\n");
            if ($cliente) {
                stream_copy_to_stream($cliente, $cliente);
                fclose($cliente);
            }
        }
  */      
        
//        echo "in ajax handler <b>";
        
/*        $fp = popen("watch -n 1 \"asterisk -rvv\"", "r");
        while($b = fgets($fp, 2048)) {
            echo $b."<br>";
            flush();
        }

        pclose($fp);
*/
$exception = "ProcessTimedOutException";
	$process = new Process("php caller/socket-server.php");
	$process->setTimeout(180);
       
    try {
        $process->run();
    } catch (ProcessTimedOutException $exception) {
        echo "Error---- Timeout, didn't call" . "<br>";
        dd("timeout");
    }
        
     //   $process->run();

        // executes after the command finishes
     /*   if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
*/
        echo $process->getOutput()."<br>";

    }
/*
$process->run();
$process->run(function ($type, $buffer) {
    if (Process::ERR === $type) {
        echo 'ERR > '.$buffer;
    } else {
        echo 'OUT > '.$buffer;
    }
});*/

}

