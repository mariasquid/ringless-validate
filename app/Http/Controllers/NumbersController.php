<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Country;

use App\Campaign;

use File;

use Illuminate\Support\Facades\Storage;

use Excel;

use App\Numbers;

use DB;

use Carbon\Carbon;

use Response;

class NumbersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$numbers = Numbers::all();
        $directorio = "/var/www/html/validate/storage/app/reports";
        $filesFolder = \File::files($directorio);
        foreach($filesFolder as $path) { 
          $files[] = pathinfo($path);
        }
        return view('numbers.index', compact('files'));
    }


        /**
        * Download file
        **/
    public function get_file($filename)
    {
        $file_path = storage_path('app/reports') . "/" . $filename;
        return Response::download($file_path);
    }

        /**
        * Delete file
        **/
    public function delete_file($filename)
    {
        $file_path = storage_path('app/reports') . "/" . $filename;
        \File::Delete($file_path);
        return redirect('/numbers')->with('success', 'The file has been deleted');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $countries = Country::all();
        $campaigns = Campaign::all();
        return view('numbers.create', compact('countries','campaigns'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //Get field from form
        $file = $request->file('numbers_phonenumbers');

        //Get name file
        $name = $file->getClientOriginalName();

        $filename = pathinfo($name, PATHINFO_FILENAME);

        $campaign = (int) $request->get('numbers_campaignid');

        $c_code = $request->get('numbers_country_code');

        //dd($campaign);
        //Store de new file in local disk
        Storage::disk('local')->put($name, File::get($file));
        $path_1 = 'app/numbers/'.$name;
        $path = 'app/numbers/'.$filename.'.csv';

        //Storage::move(Storage::get(storage_path($path_1)), Storage::get(storage_path($path)));

        $filename = str_replace("\\", "/", storage_path($path_1));

        $query = "LOAD DATA LOCAL INFILE '".$filename."' IGNORE INTO TABLE numbers (phonenumber);";

        DB::unprepared($query);

        $date = Carbon::now();

        DB::update('update ignore numbers set status = "new", campaignid = ?, country_code = ?, created_at = ?, updated_at = ? where campaignid is NULL and country_code is NULL', [$campaign,$c_code,$date,$date]);

        $amount = DB::table('numbers')
                     ->select(DB::raw('count(*) as cnt'))
                     ->where('created_at', $date)
                     ->first();

       (array)$success = 'has been added to Campaign';

       $camp = Campaign::find($campaign);

        DB::delete('delete from numbers where campaignid is NULL and country_code is NULL and status is NULL');

       $c = json_decode($camp);

       $directorio = "/var/www/html/validate/storage/app/reports";
        $filesFolder = \File::files($directorio);
        foreach($filesFolder as $path) { 
          $files[] = pathinfo($path);
        }

       return view('numbers.index',compact('amount','success','c','files'));
            
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
