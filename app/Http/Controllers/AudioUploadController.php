<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Http\File;

use Illuminate\Support\Facades\Storage;

class AudioUploadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

     /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function audioUpload()

    {

        return view('audio.upload');

    }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function audioUploadPost(Request $request)

    {

        request()->validate([

            'audio' => 'required|max:2048',

        ]);



        $rootPath = '/var/lib/asterisk/sounds/campaign';
        $client = Storage::createLocalDriver(['root' => $rootPath]);
        

        //Get field from form
        $file = $request->file('audio');

        //Get name file
        $name = $file->getClientOriginalName();
         
        $client->put($name, \File::get($file));
  
        return redirect('audio-upload')->with('success', 'You have successfully upload audio.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
