<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Campaign;


class CampaignController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $campaigns = Campaign::all();
        return view('campaign.index', compact('campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('campaign.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         //
        $request->validate([
            'campaign_name' => 'required|unique:campaign,name'
        ]);
        $campaign = new Campaign([ 
            'name' => $request->get('campaign_name'),
            'description' => $request->get('campaign_description')
        ]);
        $campaign->save();
        return redirect('/campaign')->with('success', 'Campaign has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $campaign = Campaign::find($id);
        return view('campaign.edit', compact('campaign'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /*$request->validate([
            'campaign_name' => 'required|unique:campaign,name'
        ]);*/

        $campaign = Campaign::find($id);
        $campaign->name = $request->get('campaign_name');
        $campaign->description = $request->get('campaign_description');
        $campaign->save();
        return redirect('/campaign')->with('success', 'Campaign has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $campaign = Campaign::find($id);
        $campaign->delete();
        return redirect('/campaign')->with('Success', 'campaign has been Deleted Successfully');
    }
}
