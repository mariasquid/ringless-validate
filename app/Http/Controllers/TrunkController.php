<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trunk;

class TrunkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $trunks = Trunk::orderBy('name')->get();
        return view('trunk.index', compact('trunks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('trunk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'trunk_name' => 'required|unique:trunk,name',
            'trunk_ip' => 'required|unique:trunk,ip_address',
            'trunk_connection' => 'required|unique:trunk,connection_string'
        ]);
        $trunk = new Trunk([ 
            'name' => $request->get('trunk_name'),
            'ip_address' => $request->get('trunk_ip'),
            'connection_string' => $request->get('trunk_connection')
        ]);
        $trunk->save();
        return redirect('/trunk')->with('success', 'Trunk has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $trunk = Trunk::find($id);
        return view('trunk.edit', compact('trunk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       /* $request->validate([
            'trunk_name' => 'required|unique:trunk,name',
            'trunk_ip' => 'required|unique:trunk,ip_address',
            'trunk_connection' => 'required|unique:trunk,connection_string'
        ]);*/

        $trunk = Trunk::find($id);
        $trunk->name = $request->get('trunk_name');
        $trunk->ip_address = $request->get('trunk_ip');
        $trunk->connection_string = $request->get('trunk_connection');

        $trunk->save();
        return redirect('/trunk')->with('success', 'Trunk has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $trunk = Trunk::find($id);
        $trunk->delete();
        return redirect('/trunk')->with('Success', 'Trunk has been Deleted Successfully');
    }
}
