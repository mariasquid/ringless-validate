<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Trunk;

use App\Country;

use App\Campaign;

use App\Run;

use DB;

use Carbon\Carbon;

use \Artisan;

use Symfony\Component\Process\Process;


class RunController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $running = Run::all();
        
        return view('run.index', compact('running'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $countries = Country::all();
        $campaigns = Campaign::all();
        $trunks = Trunk::orderBy('name')->get();
        //dd($countries);
        return view('run.create', compact('countries','campaigns','trunks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $date = Carbon::now();
        $trunks = Trunk::find($request->get('run_trunk_id'));
        $trunk = json_decode($trunks);
        $t_name = $trunk->name;
        $s_c = $trunk->connection_string;
        $campaigns = Campaign::find($request->get('run_campaignid'));
        $camp = json_decode($campaigns);
        $campaign_name = $camp->name;
        DB::table('run')->insert(
        ['campaignid' => $request->get('run_campaignid'), 'name_campaign' => $campaign_name, 'trunkid' => $request->get('run_trunk_id'), 'trunk_name' => $t_name, 'connection_string' => $s_c, 'cps' => $request->get('run_cps'), 'sleep_time' => $request->get('run_sleep_time'), 'callerid' => $request->get('run_callerid'), 'context' => $request->get('run_context'), 'prefix_dial'=> $request->get('run_prefix'), 'country_code' => $request->get('run_country_code'), 'timer_enabled' => 'no', 'pause' => 'off', 'status' => 'actived', 't1' => $request->get('run_t1'), 't2' => $request->get('run_t2'), 't3' => $request->get('run_t3'), 'created_at' => $date, 'updated_at' => $date ]
        );

        $running = Run::all();
        (array)$success = 'has been added Campaign to running';
        return view('run.index', compact('running','success'));
    } 

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }


    public function ami(){

        return view('run.ami');
        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pause($id)
    {
        //
        $campaign = Run::find($id);
        $camp = json_decode($campaign);
        $pid = $camp->pid_number;
        $process = new Process("kill $pid");
        $process->run();
        DB::table('run')
                ->where('campaignid', $id)
                ->update(['status' => 'deactived', 'pause' => 'on']);
        return redirect('/run')->with('success', 'Running Campaign has been paused');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function start($id)
    {
        //
        $campaign = Run::find($id);
        $camp = json_decode($campaign);
        $pid = $camp->pid_number;
        $process = new Process("kill $pid");
        $process->run();
        DB::table('run')
                ->where('campaignid', $id)
                ->update(['status' => 'actived', 'pause' => 'off']);
        return redirect('/run')->with('success', 'Running Campaign has been started');

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $running = Run::find($id);
        /*$directorio = "/var/lib/asterisk/sounds/campaign";
        $filesFolder = \File::files($directorio);
        foreach($filesFolder as $path) { 
          $audios[] = pathinfo($path);
        }*/
        $countries = Country::all();
        $campaigns = Campaign::all();
        $trunks = Trunk::orderBy('name')->get();
        return view('run.edit', compact('running','directorio','filesFolder','countries','campaigns','trunks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $date = Carbon::now();
        $trunks = Trunk::find($request->get('run_trunk_id'));
        $trunk = json_decode($trunks);
        $t_name = $trunk->name;
        $s_c = $trunk->connection_string;

        /*$pid = $request->get('run_pid');
    
        $process = new Process("kill $pid");
        $process->run();*/

        DB::table('run')
                ->where('campaignid', $id)
                ->update(['trunkid' => $request->get('run_trunk_id'), 'trunk_name' => $t_name, 'connection_string' => $s_c, 'cps' =>$request->get('run_cps'), 'sleep_time' =>$request->get('run_sleep_time'), 'callerid' =>$request->get('run_callerid'), 'context' =>$request->get('run_context'), 'prefix_dial' => $request->get('run_prefix'), 'country_code' => $request->get('run_country_code'), 'status' => 'actived', 't1' => $request->get('run_t1'), 't2' => $request->get('run_t2'), 't3' => $request->get('run_t3'), 'updated_at' => $date ]);

        return redirect('/run')->with('success', 'Running Campaign has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $campaign = Run::find($id);
        $camp = json_decode($campaign);
        $pid = $camp->pid_number;
        $process = new Process("kill $pid");
        $process->run();
        $campaign->delete();
        return redirect('/run')->with('Success', 'campaign has been Deleted Successfully');
    }
}
