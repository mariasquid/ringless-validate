<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //vj changing somethings:
     //protected $redirectTo = '/home';
    protected function redirectTo()
    {
        $userId = auth()->user()->id;
//        return '/home'; //just to test. It works.

        switch ($userId){
            case 1:
                return '/test';
                break;
            case 2:
//		dd($userId);    //este es el usuario donde estara el ringless dialer
                return  '/home';
                break;
            default:
		//dd($userId);
                return '/home';
                break;

        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
