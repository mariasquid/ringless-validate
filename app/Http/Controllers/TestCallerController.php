<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Exception\ProcessTimedOutException;
use Illuminate\Http\Request;
use App\Trunk;
use App\Country;
use App\Campaign;

class TestCallerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$directorio = "/var/lib/asterisk/sounds/campaign";
        $filesFolder = \File::files($directorio);
        foreach($filesFolder as $path) { 
          $audios[] = pathinfo($path);
        }*/
        $trunks = Trunk::orderBy('name')->get();
        //dd($countries);
        return view('test.test', compact('trunks'));
    }

    public function call(Request $request)
    {
        $number = $request->input('test_number');
        $callerid = $request->input('test_callerid');
        $trunk = $request->input('test_trunk_id');
        $prefix = $request->input('test_prefix');
        $context = $request->input('test_context');
        $phone_number = $prefix.$number;
        $trunks = Trunk::find($trunk);
        $trunk_id = json_decode($trunks);
        $t_name = $trunk_id->name;
        $s_c = $trunk_id->connection_string;
        $t1 = $request->input('test_t1');
        $t2 = $request->input('test_t2');
        $t3 = $request->input('test_t3');
    //dd($request->all());
        $process = new Process("php caller/ringless.php $phone_number $callerid $callerid $s_c $number $context $t1 $t2 $t3");
        $process->run();
        
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        return redirect('/testcaller')->with('success', 'The call was processed');
    }

    public function status()
    {
        return view('status');
    }

    public function execute_status()
    {
        set_time_limit(300);

        header("Connection: Keep-alive");
        $exception = "ProcessTimedOutException";
        $process = new Process("php caller/socket-server.php");
        $process->setTimeout(190);
        try {
            $process->run();
            echo $process->getOutput()."<br>";
        } catch (ProcessTimedOutException $exception) {
            echo "Error---- Timeout, didn't call" . "<br>";
            exit(0);
    }
        
    }

}

