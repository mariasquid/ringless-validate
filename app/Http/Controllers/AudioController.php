<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use File;

use Illuminate\Support\Facades\Storage;

use Response;

use Symfony\Component\Process\Process;

class AudioController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$numbers = Numbers::all();
        $directorio = "/var/lib/asterisk/sounds/campaign";
        $filesFolder = \File::files($directorio);
        foreach($filesFolder as $path) { 
          $files[] = pathinfo($path);
        }
        return view('audio.list', compact('files'));
    }


        /**
        * Download file
        **/
    public function play_audio($filename)
    {
        $temp_var = explode(".", $filename);
        $audio = $temp_var[0];
        $file_path = "/var/lib/asterisk/sounds/campaign/".$filename;
        $command = "ffmpeg -i " . $file_path ." /var/www/html/ringless/public/audios/". $audio .".mp3";
        //echo $command;
        $process = new Process($command);
        $process->run();
        //dd($process);
        $mp3_path = "audios/".$audio.".mp3";
        $filesize = (int) File::size($mp3_path);
        $file = File::get($mp3_path);
        $response = Response::make($file, 200);
        $response->header('Content-Type', 'audio/mpeg');
        $response->header('Content-Length', $filesize);
        $response->header('Accept-Ranges', 'bytes');
        $response->header('Content-Range', 'bytes 0-'.$filesize.'/'.$filesize);
        return $response;
    }

        /**
        * Delete file
        **/
    public function delete_file($filename)
    {
        $temp_var = explode(".", $filename);
        $audio = $temp_var[0];
        $file_path = "/var/lib/asterisk/sounds/campaign/".$filename;
        $mp3_path = "audios/".$audio.".mp3";
        File::Delete($file_path);
        File::Delete($mp3_path);
        return redirect('/audio-list')->with('success', 'The file has been deleted');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $countries = Country::all();
        $campaigns = Campaign::all();
        return view('numbers.create', compact('countries','campaigns'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //Get field from form
        $file = $request->file('numbers_phonenumbers');

        //Get name file
        $name = $file->getClientOriginalName();

        $filename = pathinfo($name, PATHINFO_FILENAME);

        $campaign = (int) $request->get('numbers_campaignid');

        $c_code = $request->get('numbers_country_code');

        //dd($campaign);
        //Store de new file in local disk
        Storage::disk('local')->put($name, File::get($file));
        $path_1 = 'app/numbers/'.$name;
        $path = 'app/numbers/'.$filename.'.csv';

        //Storage::move(Storage::get(storage_path($path_1)), Storage::get(storage_path($path)));

        $filename = str_replace("\\", "/", storage_path($path_1));

        $query = "LOAD DATA LOCAL INFILE '".$filename."' IGNORE INTO TABLE numbers (phonenumber);";

        DB::unprepared($query);

        $date = Carbon::now();

        DB::update('update ignore numbers set status = "new", campaignid = ?, country_code = ?, created_at = ?, updated_at = ? where campaignid is NULL and country_code is NULL', [$campaign,$c_code,$date,$date]);

        $amount = DB::table('numbers')
                     ->select(DB::raw('count(*) as cnt'))
                     ->where('created_at', $date)
                     ->first();

       (array)$success = 'has been added to Campaign';

       $camp = Campaign::find($campaign);

        DB::delete('delete from numbers where campaignid is NULL and country_code is NULL and status is NULL');

       $c = json_decode($camp);

       return view('numbers.index',compact('amount','success','c'));
            
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
