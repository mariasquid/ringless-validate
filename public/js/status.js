function executeAsteriskCli(){
    console.log('say execute ajax');

    if (!window.XMLHttpRequest){
        alert("Your browser does not support the native XMLHttpRequest object.");
        return;
    }
    try{
        var xhr = new XMLHttpRequest();
        xhr.previous_text = '';

        xhr.onerror = function() {
            alert("[XHR] Fatal Error.");
        };
        xhr.onreadystatechange = function() {
            try{
                if (xhr.readyState == 4){
                    //alert('[XHR] Done')  //this is executed at end
                    document.getElementById("status-area").innerHTML += 'Done' + '';
                }else if (xhr.readyState > 2){
                    console.warn(xhr.responseText);
                    var new_response = xhr.responseText.substring(xhr.previous_text.length);
                    document.getElementById("status-area").innerHTML += new_response + '';
                    xhr.previous_text = xhr.responseText;
                }
            }
            catch (e){
                alert("[XHR STATECHANGE] Exception: " + e);
            }
        };
        xhr.open("GET", "/execute-status", true);
        xhr.send();
    }
    catch (e){
        alert("[XHR REQUEST] Exception: " + e);
    }

}