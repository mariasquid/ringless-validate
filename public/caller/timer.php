<?php

/*
CLass: Timer, like arduino timer
https://stackoverflow.com/questions/8310487/start-and-stop-a-timer-php

*/

class Timer {
	var $classname = "Timer";
	var $start	= 0;
	var $stop	= 0;
	var $elapsed	= 0;
	var $deltaT	= 0;

	#Constructor
	function Timer($wait){ //$start = true 
		//if ($start)
		//	$this->start();
        $this->deltaT = $wait;
        echo $this->deltaT;
	}

	#Start counting time
	function start(){
		$this->start = $this->_gettime();
		$this->started = true;
	}

	#Stop counting time
	function stop(){
		$this->stop = $this->_gettime();
		$this->elapsed = $this->_compute();
	}

	#Get eleapsed time
	function elapsed(){
		$melapsed = 0;
		if($this->start){
			//if(!$this->elapsed)
			$this->stop();
			$melapsed = $this->elapsed;
		}
		else {
			$melapsed = 0;
		}
		
		return $melapsed;
	}

	#Reset timer so it can be used again
	function reset(){
		$this->start	= 0;
		$this->stop	= 0;
		$this->elapsed	= 0;
	}

	#If elapsed time > $wait, returns true
	function done(){
		$done = false;
		if($this->deltaT && $this->start){
			$melapsed = $this->elapsed();
			if($this->deltaT < $melapsed){
				$done = true;
			}
		}
		return $done;
	}

	##Private Methods

	#Get current time
	function _gettime(){
		$mtime = microtime();
		$mtime = explode(" ", $mtime);
		return $mtime[1] + $mtime[0];
	}

	#Compute elapsed time
	function _compute(){
		return $this->stop - $this->start;
	}
}

?>

