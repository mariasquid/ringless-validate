<?php
/* These are (in order) the options we can pass to pami client:
 *
 * The hostname or ip address where asterisk ami is listening
 * The scheme can be tcp:// or tls://
 * The port where asterisk ami is listening
 * Username configured in manager.conf
 * Password configured for the above user
 * Connection timeout in milliseconds
 * Read timeout in milliseconds
 */

require 'vendor/autoload.php';

use PAMI\Client\Impl\ClientImpl as PamiClient;
use PAMI\Message\Event\EventMessage;
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\NewchannelEvent;
use PAMI\Message\Event\OriginateResponseEvent;

//NewCalleridEvent;

$pamiClientOptions = array(
 'host' => '127.0.0.1',
 'scheme' => "tcp://",
 'port' => 5038,
 'username' => 'validate',
 'secret' => 'B0n4mp4rK',
 'connect_timeout' => 10000,
 'read_timeout' => 10000
);

//var_dump($pamiClientOptions);


$pamiClient = new PamiClient($pamiClientOptions);
// Open the connection
$pamiClient->open();
// Close the connection
sleep(2);
//new listener
/*//this is the listener to all events. It works
$pamiClient->registerEventListener(function (EventMessage $event) {
    var_dump($event);
});
*/
//using predicates to filter events
$pamiClient->registerEventListener(
    function (EventMessage $newChannelEvent) {
//        var_dump($event);
	//here go app code
	$channelId = $newChannelEvent->getChannel();
	var_dump($channelId);

    },
    function (EventMessage $newChannelEvent) {
        return
            $newChannelEvent instanceof NewchannelEvent; // && $event->getSubEvent() == 'Begin';
    }
);

$pamiClient->registerEventListener(
    function (EventMessage $originateResponseEvent) {
//        var_dump($event);
	//here go app code
	$actionId = $originateResponseEvent->getActionID();
	$status = $originateResponseEvent->getResponse();
	$callerid = $originateResponseEvent->getChannel();
	var_dump($actionId);
	var_dump($status);
	var_dump($callerid);
    },
    function (EventMessage $originateResponseEvent) {
        return
            $originateResponseEvent instanceof OriginateResponseEvent;
    }
);


$running = true;
// Main loop
while($running) {
    $pamiClient->process();
    usleep(500);
}
//end listener and process
echo 'close conn';
$pamiClient->close();
?>


