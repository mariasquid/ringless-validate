<?php
/* These are (in order) the options we can pass to pami client:
 *
 * The hostname or ip address where asterisk ami is listening
 * The scheme can be tcp:// or tls://
 * The port where asterisk ami is listening
 * Username configured in manager.conf
 * Password configured for the above user
 * Connection timeout in milliseconds
 * Read timeout in milliseconds
 */

require 'vendor/autoload.php';
require 'timer.php';

use PAMI\Client\Impl\ClientImpl as PamiClient;
use PAMI\Message\Event\EventMessage;
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\OriginateResponseEvent;
use PAMI\Message\Event\NewchannelEvent;
use PAMI\Message\Action\OriginateAction;
use PAMI\Message\Action\HangupAction;
//NewCalleridEvent;

$channelId = "";

$pamiClientOptions = array(
 'host' => '127.0.0.1',
 'scheme' => "tcp://",
 'port' => 5038,
 'username' => 'validate',
 'secret' => 'B0n4mp4rK',
 'connect_timeout' => 10000,
 'read_timeout' => 10000
);

$pamiClient = new PamiClient($pamiClientOptions);
// Open the connection
$pamiClient->open();
sleep(2);
//timer
$timer1 = new Timer(7.001);
//new listener
//using predicates to filter events
$pamiClient->registerEventListener(
    function (EventMessage $newChannelEvent) {
//        var_dump($event);
        //here go app code
        global $channelId;
	$channelId = $newChannelEvent->getChannel();
        var_dump($channelId);

    },
    function (EventMessage $newChannelEvent) {
	global $timer1;
        $timer1->start();
        return
            $newChannelEvent instanceof NewchannelEvent; // && $event->getSubEvent() == 'Begin';
    }
);

//Call1 info
$type = "SIP";
$trunk = "checkbox";
$context ="just"; //"generic";
$extension = "s";
$number = "17865782636";
$callerId = "7865782633";
$async = "yes";
$actionid = "firstCall";

//Create Originate Message1
$originateMsg = new OriginateAction($type . "/" . $trunk . "/" . $number);
$originateMsg->setContext($context);
$originateMsg->setPriority('1');
$originateMsg->setExtension($extension);
$originateMsg->setCallerId($callerId);
$originateMsg->setAsync($async);
$originateMsg->setActionID($actionid);
$pamiClient->send($originateMsg);

//Call2 info
$type = "SIP";
$trunk = "checkbox";
$context ="just"; //"generic";
$extension = "s";
$number = "17865782636";
$callerId = "7865782634";
$async = "yes";
$actionid = "secondCall";

//Create Originate Message2
$originateMsg2 = new OriginateAction($type . "/" . $trunk . "/" . $number);
$originateMsg2->setContext($context);
$originateMsg2->setPriority('1');
$originateMsg2->setExtension($extension);
$originateMsg2->setCallerId($callerId);
$originateMsg2->setAsync($async);
$originateMsg2->setActionID($actionid);
//$pamiClient->send($originateMsg2);

$running = true;
// Main loop
while($running) {
    $pamiClient->process();
    if($timer1->done()){
	echo "timeout hangUp";
        $pamiClient->send(new HangupAction($channelId));
	break;
    }
    usleep(1000);
}

$pamiClient->send($originateMsg2);
$pamiClient->process();

//end listener and process
// Close the connection
echo 'close conn';
$pamiClient->close();
?>


