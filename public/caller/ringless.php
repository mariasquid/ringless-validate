<?php
/* These are (in order) the options we can pass to pami client:
 *
 * The hostname or ip address where asterisk ami is listening
 * The scheme can be tcp:// or tls://
 * The port where asterisk ami is listening
 * Username configured in manager.conf
 * Password configured for the above user
 * Connection timeout in milliseconds
 * Read timeout in milliseconds
 */

require 'vendor/autoload.php';
require 'timer.php';

use PAMI\Client\Impl\ClientImpl as PamiClient;
use PAMI\Message\Event\EventMessage;
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\OriginateResponseEvent;
use PAMI\Message\Event\NewchannelEvent;
use PAMI\Message\Action\OriginateAction;
use PAMI\Message\Action\HangupAction;
use PAMI\Message\Message;
//NewCalleridEvent;


$number = $argv[1]; 
$callerId1 = $argv[2]; 
$callerId2 = $argv[3]; 
$trunk = $argv[4];
$num = $argv[5];
$context = $argv[6];
$t1 = $argv[7];
$t2 = $argv[8];
$t3 = $argv[9];

$channelId = "";
$firstCallf = true;
$secondCallDone = false;

$pamiClientOptions = array(
 'host' => '127.0.0.1',
 'scheme' => "tcp://",
 'port' => 5038,
 'username' => 'validate',
 'secret' => 'B0n4mp4rK',
 'connect_timeout' => 100000,
 'read_timeout' => 100000
);

$pamiClient = new PamiClient($pamiClientOptions);
// Open the connection
$pamiClient->open();
sleep(2);
//timer
$default1 = 5.999999999999999999;
$default2 = 2.222222222222222222;
$default3 = 12.22222111111111111;
if(isset($t1, $t2, $t3)){
	$timer1 =  new Timer($t1);
	$timer2ndCall = new Timer($t2);
	$timerend2ndCall = new Timer($t3);
}else{
	$timer1 = new Timer($default1);
	$timer2ndCall = new Timer($default2);
	$timerend2ndCall = new Timer($default3);
}
//new listener
//using predicates to filter events
$pamiClient->registerEventListener(
    function (EventMessage $newChannelEvent) {
//        var_dump($event);
        //here go app code
        global $channelId;
        global $channelId2;
        global $firstCallf;
        global $secondCallf; 
        if($firstCallf){
            $channelId = $newChannelEvent->getChannel();
            $firstCallf = false;
            $secondCallf = true; 
        }
	    //var_dump($channelId);
        if($secondCallf){
            $channelId2 = $newChannelEvent->getChannel();
             if($channelId2 !== $channelId){
                $secondCallf = false;
            var_dump($channelId);
            var_dump($channelId2);

             }

        }
         $variables = $newChannelEvent->getChannelVariables($channelId2);
            var_dump($variables);

    },
    function (EventMessage $newChannelEvent) {
        global $timer1;
        global $firstCallf;
        if($firstCallf){
            $timer1->start();
        }
        return
            $newChannelEvent instanceof NewchannelEvent; // && $event->getSubEvent() == 'Begin';
    }
);

//Call1 info
$type = "SIP";
//$context ="amd"; //"just" "generic";
$extension = "s";
$async = "yes";
$actionid = "firstCall";

//Create Originate Message1
$originateMsg = new OriginateAction($trunk . $number);
$originateMsg->setContext($context);
$originateMsg->setPriority('1');
$originateMsg->setExtension($extension);
$originateMsg->setCallerId($callerId1);
$originateMsg->setVariable('number', $num);
$originateMsg->setAsync($async);
$originateMsg->setActionID($actionid);
//$pamiClient->send($originateMsg);
$orgresp = $pamiClient->send($originateMsg);
$orgStatus = $orgresp->getKeys()['response'];
echo $orgStatus;

//Call2 info
$type = "SIP";
//$context ="amd"; //"generic""just";
$extension = "s";
$async = "yes";
$actionid = "secondCall";

//Create Originate Message2
$originateMsg2 = new OriginateAction($trunk . $number);
$originateMsg2->setContext($context);
$originateMsg2->setPriority('1');
$originateMsg2->setExtension($extension);
$originateMsg2->setCallerId($callerId2);
$originateMsg2->setVariable('number', $num);
$originateMsg2->setAsync($async);
$originateMsg2->setActionID($actionid);
//$pamiClient->send($originateMsg2);

$running = true;
// Main loop
$timer2ndCall->start();
$flag = true;
while($running) {
    $pamiClient->process();
    if(!$secondCallDone && $timer2ndCall->done()){
        //echo "2nd call \n";
        $orgresp = $pamiClient->send($originateMsg2);
        $orgStatus = $orgresp->getKeys()['response'];
        echo $orgStatus;
        $timerend2ndCall->start();
        $secondCallDone = true;      
    }
    if($timer1->done() && $flag == true){
        echo "timeout hangUp";
        $status = $pamiClient->send(new HangupAction($channelId));
        if(!empty($status)){
                echo "DIALED";
        }
       $flag = false;
//      break;
    }
    if($timerend2ndCall->done()){
        echo "timeout hangUp call 2";
        $status = $pamiClient->send(new HangupAction($channelId2));
        break;
    }

    usleep(1000);
}

//$pamiClient->send($originateMsg2);
//$pamiClient->process();
//end listener and process
// Close the connection
//echo 'close conn';
$pamiClient->close();
?>



