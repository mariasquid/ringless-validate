<?php
/* These are (in order) the options we can pass to pami client:
 *
 * The hostname or ip address where asterisk ami is listening
 * The scheme can be tcp:// or tls://
 * The port where asterisk ami is listening
 * Username configured in manager.conf
 * Password configured for the above user
 * Connection timeout in milliseconds
 * Read timeout in milliseconds
 */

require 'vendor/autoload.php';

use PAMI\Client\Impl\ClientImpl as PamiClient;
use PAMI\Message\Event\EventMessage;
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\OriginateResponseEvent;
use PAMI\Message\Action\OriginateAction;
//NewCalleridEvent;

$pamiClientOptions = array(
 'host' => '127.0.0.1',
 'scheme' => "tcp://",
 'port' => 5038,
 'username' => 'validate',
 'secret' => 'B0n4mp4rK',
 'connect_timeout' => 10000,
 'read_timeout' => 10000
);

//var_dump($pamiClientOptions);

$pamiClient = new PamiClient($pamiClientOptions);
// Open the connection
$pamiClient->open();
sleep(2);
//new listener
/*//this is the listener to all events. It works
$pamiClient->registerEventListener(function (EventMessage $event) {
    var_dump($event);
});
*/
//using predicates to filter events
$pamiClient->registerEventListener(
    function (EventMessage $event) {
//        var_dump($event);
	//here go app code
	$actionId = $event->getActionID();
	var_dump($actionId);
    },
    function (EventMessage $event) {
        return
            $event instanceof OriginateResponseEvent;
    }
);

//Call info
$type = "SIP";
$trunk = "checkbox";
$context ="just"; //"generic";
$extension = "s";
$number = "17865782636";
$callerId = "7865782633";
$async = "yes";
$actionid = "firstCall";

//Create Originate Message
$originateMsg = new OriginateAction($type . "/" . $trunk . "/" . $number);
$originateMsg->setContext($context);
$originateMsg->setPriority('1');
$originateMsg->setExtension($extension);
$originateMsg->setCallerId($callerId);
$originateMsg->setAsync($async);
$originateMsg->setActionID($actionid);
$pamiClient->send($originateMsg);

$running = true;
// Main loop
//while($running) {
    $pamiClient->process();
    usleep(1000);
//}
//end listener and process
// Close the connection
echo 'close conn';
$pamiClient->close();
?>

