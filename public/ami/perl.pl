#!/usr/bin/perl
use warnings;
use Asterisk::AMI;
my $astman = Asterisk::AMI->new(PeerAddr        =>      '127.0.0.1', #Remote host address
                                PeerPort        =>      '5038',      #Remote host port
                                                                     #AMI is available on TCP port 5038 if you enable it in manager.conf. 
                                Username        =>      'validate',     #Username to access the AMI

                                Secret          =>      'B0n4mp4rK' #Secret used to connect to AMI
                                );

die "Unable to connect to asterisk" unless ($astman);

my $action = $astman->send_action({ Action => 'Command',
                         Command => 'core show channels'
                       });

print $action;

