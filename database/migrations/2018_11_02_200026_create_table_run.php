<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRun extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('run', function (Blueprint $table) {
            $table->integer('campaignid')->unsigned()->primary();
            $table->string('name_campaign')->nullable();
            $table->integer('trunkid')->unsigned();
            $table->string('trunk_name',100);
            $table->string('country_code',5);
            $table->string('connection_string');
            $table->integer('sleep_time')->unsigned();
            $table->string('timer_enabled');
            $table->string('status',10);
            $table->string('pause',10);
            $table->integer('cps');
            $table->string('context',30);
            $table->string('callerid',15);
            $table->dateTime('start_time')->nullable();
            $table->dateTime('stop_time')->nullable();
            $table->dateTime('start_break')->nullable();
            $table->dateTime('stop_break')->nullable();
            $table->bigInteger('available_numbers')->nullable();
            $table->bigInteger('total_numbers')->nullable();
            $table->string('prefix_dial',15)->nullable();
            $table->string('pid_number',10)->nullable();
            $table->string('t1',20)->default('5.999999999999999999');
            $table->string('t2',20)->default('2.222222222222222222');
            $table->string('t3',20)->default('12.22222111111111111');

            $table->foreign('campaignid')->references('id')->on('campaign')->onDelete('cascade');
            $table->foreign('trunkid')->references('id')->on('trunk')->onDelete('cascade');
            $table->foreign('country_code')->references('country_code')->on('country');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('run');
    }
}
