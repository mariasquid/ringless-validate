<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTableDnc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dnc', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('phonenumber',15);
            $table->string('name_dnc',100);
            $table->integer('campaignid')->unsigned();
            $table->string('country_code',5);
            $table->timestamps();
            $table->primary(array('phonenumber', 'campaignid', 'country_code'));
            $table->foreign('campaignid')->references('id')->on('campaign')->onDelete('cascade');
            $table->foreign('country_code')->references('country_code')->on('country')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dnc');
    }
}
