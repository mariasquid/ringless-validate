<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNumbers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numbers', function (Blueprint $table) {
            $table->string('phonenumber',15)->primary();
            $table->string('status',25)->nullable();
            $table->integer('campaignid')->unsigned()->nullable()->index();
            $table->string('country_code',5)->nullable()->index();
            $table->string('amd_status',20)->nullable();
            $table->datetime('date_dialed')->nullable();
            $table->timestamps();
            $table->unique(array('phonenumber', 'campaignid', 'country_code'));
            $table->foreign('campaignid')->references('id')->on('campaign')->onDelete('cascade');
            $table->foreign('country_code')->references('country_code')->on('country')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numbers');
    }
}
