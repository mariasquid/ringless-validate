@extends('adminlte::page')

@section('title', 'List Numbers')

@section('content_header')
    <h1>Generated Reports of Numbers with answer MACHINE </h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}  
    </div><br />
  @endif
  @isset($amount)
  <div class="alert alert-success">
      @foreach ($amount as $cnt)
        <strong> {{ $cnt }} Numbers </strong>
      @endforeach
  @endisset
  @isset($success)
        {{$success}}
  @endisset
  @isset($c)
        <strong>{{ $c->name }}</strong>
    </div>
  @endisset
        
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Filename</td>
          <td colspan="3">Actions</td>
        </tr>
    </thead>
    <tbody>
      @isset($files)
        @foreach($files as $file)
        <tr>
            <td>{{$file['basename']}}</td>
            <td><a href="{{ route('numbers.download',$file['basename'])}}" class="btn btn-primary">Download</a></td>
            <td><a href="{{ route('numbers.del',$file['basename'])}}" class="btn btn-danger">Delete</a></td>
        </tr>
        @endforeach
      @endisset
    </tbody>
  </table>
   
  </table>
<div>
@stop