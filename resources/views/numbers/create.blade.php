@extends('adminlte::page')

@section('title', 'Create Numbers')

@section('content_header')
    <h1>Add Numbers</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Numbers
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('numbers.store') }}" enctype="multipart/form-data">
          <div class="form-group">
              @csrf
              <label for="numbers_country_code">Country :</label>
              <select name="numbers_country_code">
                @foreach ($countries as $country)
                  <option value="{{ $country->country_code }} "> {{$country->name}}</option>
                @endforeach 
              </select>
          </div>

          <div class="form-group">
              <label for="numbers_campaign">Campaign :</label>
              <select name="numbers_campaignid">
                @foreach ($campaigns as $campaign)
                  <option value="{{ $campaign->id }} "> {{$campaign->name}}</option>
                @endforeach 
              </select>
          </div>

          <div class="form-group">
              <label for="numbers_phonenumbers">Phonenumbers File :</label>
              <input type="file" name="numbers_phonenumbers"  class="form-control">
          </div>
              
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@stop

@section('css')
    
@stop

@section('js')
    
@stop