@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">RingLess Test Interface</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Calling....<br> Wait for status...
                </div>
                <div id="status-area">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                <script src="{{asset('js/status.js')}}"></script>

        <script>
            //When the page has loaded.
            $( document ).ready(function(){
                //Perform Ajax request.
                console.log("documente ready");
                executeAsteriskCli();
            });
        </script>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
