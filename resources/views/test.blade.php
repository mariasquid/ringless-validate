@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">RingLess Test Interface</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
                <div>
                    <form method="POST" action="{{action('TestController@call')}}">
                        <div class="form-group">
                            @csrf
                            <label >Phone Number:</label>
                            <input type="number" class="form-control" name="phone_number"/>
                        </div>
                        <div class="form-group">
                            <label >Caller ID 1:</label>
                            <input type="number" class="form-control" name="caller_id1"/>
                        </div>
                        <div class="form-group">
                            <label >Caller ID 2:</label>
                            <input type="number" class="form-control" name="caller_id2"/>
                        </div>
                        <!-- Select With One Default -->
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="trunk" class="control-label">Select trunk:</label>
                            </div>
                            <div class="col-sm-10">
                                <select class="form-control" name="trunk">
                                    <option value="checkbox">checkbox</option>
                                    <option value="voipinvite">voipinvite</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Call</button>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
