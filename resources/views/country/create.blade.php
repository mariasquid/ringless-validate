@extends('adminlte::page')

@section('title', 'Create Country')

@section('content_header')
    <h1>Create Country</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Country
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('country.store') }}">
          <div class="form-group">
              @csrf
              <label for="country_code">Country code:</label>
              <input type="text" class="form-control" name="country_code"/>
          </div>
          <div class="form-group">
              <label for="country_name">Country Name :</label>
              <input type="text" class="form-control" name="country_name"/>
          </div>
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@stop

@section('css')
    
@stop

@section('js')
    
@stop