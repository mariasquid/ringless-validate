@extends('adminlte::page')

@section('title', 'List Countries')

@section('content_header')
    <h1>Countries </h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Code Country</td>
          <td>Country Name</td>
          
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($countries as $country)
        <tr>
            <td>{{$country->country_code}}</td>
            <td>{{$country->name}}</td>
            <td><a href="{{ route('country.edit', $country->country_code )}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('country.destroy', $country->country_code)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@stop