@extends('adminlte::page')

@section('title', 'Update Country')

@section('content_header')
    <h1>Update Country</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Country
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('country.update', $country->country_code) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label for="country_name">Country Name :</label>
          <input type="text" class="form-control" name="country_name" value={{ $country->name }} />
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop