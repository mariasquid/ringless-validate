@extends('adminlte::page')

@section('title', 'Update Campaign')

@section('content_header')
    <h1>Update Campaign</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Trunk
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('campaign.update', $campaign->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label for="name">Campaign Name:</label>
          <input type="text" class="form-control" name="campaign_name" value={{ $campaign->name }} />
        </div>
        <div class="form-group">
          <label for="trunk_ip">Campaign Description :</label>
          <input type="text" class="form-control" name="campaign_description" value={{ $campaign->description }} />
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@stop

@section('css')
    
@stop

@section('js')
    
@stop