@extends('adminlte::page')

@section('title', 'List Campaigns')

@section('content_header')
    <h1>Campaigns </h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Campaing Name</td>
          <td>Campaign Description</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($campaigns as $campaign)
        <tr>
            <td>{{$campaign->id}}</td>
            <td>{{$campaign->name}}</td>
            <td>{{$campaign->description}}</td>
            <td><a href="{{ route('campaign.edit',$campaign->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('campaign.destroy', $campaign->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@stop