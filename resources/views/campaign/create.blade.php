@extends('adminlte::page')

@section('title', 'Create Campaign')

@section('content_header')
    <h1>Create Campaign</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Campaign
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('campaign.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Campaign Name:</label>
              <input type="text" class="form-control" name="campaign_name"/>
          </div>
          <div class="form-group">
              <label for="ip">Campaign Description :</label>
              <input type="text" class="form-control" name="campaign_description"/>
          </div>
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@stop

@section('css')
    
@stop

@section('js')
  
@stop