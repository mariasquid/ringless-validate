@extends('adminlte::page')

@section('title', 'Create Trunk')

@section('content_header')
    <h1>Create Trunk</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Trunk
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('trunk.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Trunk Name:</label>
              <input type="text" class="form-control" name="trunk_name"/>
          </div>
          <div class="form-group">
              <label for="ip">Trunk IP Address :</label>
              <input type="text" class="form-control" name="trunk_ip"/>
          </div>
          <div class="form-group">
              <label for="connection">Trunk Connection String :</label>
              <input type="text" class="form-control" name="trunk_connection"/>
          </div>
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop