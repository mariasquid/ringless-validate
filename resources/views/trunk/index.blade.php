@extends('adminlte::page')

@section('title', 'List Trunks')

@section('content_header')
    <h1>Trunks </h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Trunk Name</td>
          <td>Trunk IP Address</td>
          <td>Trunk Connection String</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($trunks as $trunk)
        <tr>
            <td>{{$trunk->id}}</td>
            <td>{{$trunk->name}}</td>
            <td>{{$trunk->ip_address}}</td>
            <td>{{$trunk->connection_string}}</td>
            <td><a href="{{ route('trunk.edit',$trunk->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('trunk.destroy', $trunk->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@stop