@extends('adminlte::page')

@section('title', 'Update Trunk')

@section('content_header')
    <h1>Update Trunk</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Trunk
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('trunk.update', $trunk->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label for="name">Trunk Name:</label>
          <input type="text" class="form-control" name="trunk_name" value={{ $trunk->name }} />
        </div>
        <div class="form-group">
          <label for="trunk_ip">Trunk IP Address :</label>
          <input type="text" class="form-control" name="trunk_ip" value={{ $trunk->ip_address }} />
        </div>
        <div class="form-group">
          <label for="connection_string">Trunk Connection String:</label>
          <input type="text" class="form-control" name="trunk_connection" value={{ $trunk->connection_string }} />
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop