@extends('adminlte::page')

@section('title', 'Campaign Audio')

@section('content_header')
    <h1>Upload Campaign Audio</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <div class="card-header">
    Upload Audio Campaign
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" enctype="multipart/form-data" action="{{ route('audio.upload.post') }}">
          <div class="form-group">
              @csrf
              <label for="name">Campaign Audio :</label>
              <input type="file" name="audio"  class="form-control">
          </div>
          <button type="submit" class="btn btn-primary">Upload</button>
      </form>
  </div>
</div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop