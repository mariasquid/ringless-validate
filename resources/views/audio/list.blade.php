@extends('adminlte::page')

@section('title', 'List Audios')

@section('content_header')
    <h1>List Audios</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Audio name</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
      @forelse ($files as $audio)
        <tr>
            <td>{{$audio['basename']}}</td>
            <td><a href="{{ route('audio.play', $audio['basename'] )}}" class="btn btn-primary">Play</a></td>
            <td><a href="{{ route('audio.del', $audio['basename'] )}}" class="btn btn-danger">Delete</a></td>
        </tr>
        @empty
        <p>No audios here</p>
      @endforelse
    </tbody>
  </table>
<div>
@stop