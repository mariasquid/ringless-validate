@extends('adminlte::page')

@section('title', 'Run Campaign')

@section('content_header')
    <h1>Run Campaign</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
   @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}  
    </div><br />
    @endif
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
  <div class="card-header">
    Create running Campaign
  </div>
  <div class="card-body">
      <form method="post" action="{{ route('run.store') }}">
          <div class="form-group">
            @csrf
              <label for="run_campaignid">Campaign :</label>
              <select name="run_campaignid">
                @foreach ($campaigns as $campaign)
                  <option value="{{ $campaign->id }} "> {{$campaign->name}}</option>
                @endforeach 
              </select>
          </div>

          <div class="form-group">
              <label for="run_trunk_id">Trunk :</label>
              <select name="run_trunk_id">
                @foreach ($trunks as $trunk)
                  <option value="{{ $trunk->id }} "> {{$trunk->name}}</option>
                @endforeach 
              </select>
          </div>

          <div class="card-header">
            <strong>Call Settings</strong>
          </div>
          <div class="form-group">
              <label for="run_cps">CPS : </label>
              <select name="run_cps">
              <?php
                for($i=1;$i<=30;$i++){
                  echo "<option value=".$i.">". $i."</option>";
                }
              ?>
              </select>
          </div>
          <div class="form-group">
              <label for="run_sleep_time">Sleep Time : </label>
              <select name="run_sleep_time">
              <?php
                for($i=1;$i<=30;$i++){
                  echo "<option value=".$i.">". $i."</option>";
                }
              ?>
              </select>
          </div>
          <div class="form-group">
              <label for="run_callerid">Caller ID :</label>
              <input type="input" name="run_callerid"  class="form-control">
          </div>
          <div class="form-group">
              <label for="run_context">Use Context : </label>
              <select name="run_context">
                  <option value="amd2">amd2 context</option>";
              </select>
          </div>
          <div class="form-group">
              <label for="run_prefix"> Prefix :</label>
              <input type="input" name="run_prefix"  class="form-control">
          </div>
          <div class="form-group">
              <label for="run_country_code">Country :</label>
              <select name="run_country_code">
                @foreach ($countries as $country)
                  <option value="{{ $country->country_code }}">{{$country->name}}</option>
                @endforeach 
              </select>
          </div>
          <div class="form-group">
              <label> Adjust time between calls (optional) :</label>
          </div>
          <div class="form-group">
              <label for="run_t1"> Duration of the First Call :</label>
              <input type="input" name="run_t1"  class="form-control" value="5.999999999999999999">
          </div>
          <div class="form-group">
              <label for="run_t2"> Elapsed Time Before Starting the Second Call :</label>
              <input type="input" name="run_t2"  class="form-control" value="2.222222222222222222">
          </div>
          <div class="form-group">
              <label for="run_t3"> Duration of the Second Call :</label>
              <input type="input" name="run_t3"  class="form-control" value="12.22222111111111111">
          </div>

          </div>
          <button type="submit" class="btn btn-primary">Add</button>
      </form>
  </div>
</div>
@stop

@section('css')
    
@stop

@section('js')
  
@stop
