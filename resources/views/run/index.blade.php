@extends('adminlte::page')

@section('title', 'List Campaigns')

@section('content_header')
    <h1>Campaigns </h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}  
    </div><br />
    @endif
    @isset($success)
      <div class="alert alert-success">
        {{$success}}
      </div><br />
    @endisset
<div class="box">
  <div class="box-body table-responsive no-padding">
    <table class="table table-striped">
    <thead>
        <tr>
          <td><strong>Campaign</strong></td>
          <td><strong>Trunk</strong></td>
          <td><strong>Connection String</strong></td>
          <td><strong>Country</strong></td>
          <td><strong>Pause</strong></td>
          <td><strong>Sleep Time</strong></td>
          <td><strong>CPS</strong></td>
          <td><strong>Numbers</strong></td>
          <td><strong>Prefix Dial</strong></td>
          <td colspan="4"><strong>Actions</strong></td>
        </tr>
    </thead>
    <tbody>
        @foreach($running as $run)
        <tr>
            <td>{{$run->name_campaign}}</td>
            <td>{{$run->trunk_name}}</td>
            <td>{{$run->connection_string}}</td>
            <td>{{$run->country_code}}</td>
            <td>{{$run->pause}}</td>
            <td>{{$run->sleep_time}}</td>
            <td>{{$run->cps}}</td>
            <td>{{$run->available_numbers}}</td>
            <td>{{$run->prefix_dial}}</td>
            <td><a href="{{ route('run.edit',$run->campaignid)}}" class="btn btn-primary">Edit</a></td>
            <td><a href="{{ route('run.pause',$run->campaignid)}}" class="btn btn-primary">Pause</a></td>
            <td><a href="{{ route('run.start',$run->campaignid)}}" class="btn btn-primary">Start</a></td>
            <td>
                <form action="{{ route('run.destroy', $run->campaignid)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Stop</button>
                </form>
            </td>
            
        </tr>
        @endforeach
    </tbody>
  </table>
  </div>
</div>
<div class="box">
  <div class="box-body table-responsive">
    <table class="table table-striped">
      <tbody>
        <tr>
          <td>
            <button onclick="load_channels()" type="button">Show Channels</button>
            <div class="row" id="ami_div">
              
            </div>
          </td>
        </tr>
      </tbody>
    </table>  
  </div>
</div>
@stop
@section('js')
    <script type="text/javascript">
    function load_channels()
    {
        $('#ami_div').load('ami-channels');
    }
</script>
@stop