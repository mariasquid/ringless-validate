@extends('adminlte::page')

@section('title', 'Test Call')

@section('content_header')
    <h1>Test Call</h1>
@stop

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}  
    </div><br />
    Calling....<br> Wait for status...<br><br>
  <div id="status-area">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{asset('js/status.js')}}"></script>
  <script>
            //When the page has loaded.
      $( document ).ready(function(){
        //Perform Ajax request.
        console.log("documente ready");
        executeAsteriskCli();
      });
  </script>

  </div><br>
    @endif
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
  <div class="card-header">
    @if (session('status'))
      <div class="alert alert-success" role="alert">
        {{ session('status') }}
      </div>
    @endif

  </div>

  <div class="card-body">
    
      <form method="post" action="{{ route('testcall') }}">
            @csrf
          <div class="form-group">
              <label for="test_trunk_id">Trunk :</label>
              <select name="test_trunk_id">
                @foreach ($trunks as $trunk)
                  <option value="{{ $trunk->id }} "> {{$trunk->name}}</option>
                @endforeach 
              </select>
          </div>

          <div class="form-group">
              <label for="test_number">Number :</label>
              <input type="input" name="test_number"  class="form-control">
          </div>
          
          <div class="form-group">
              <label for="test_callerid">Caller ID :</label>
              <input type="input" name="test_callerid"  class="form-control">
          </div>
          <div class="form-group">
              <label for="test_context">Use Context : </label>
              <select name="test_context">
                  <option value="amd2">amd2 context</option>";
              </select>
          </div>
          <div class="form-group">
              <label for="test_prefix"> Prefix :</label>
              <input type="input" name="test_prefix"  class="form-control">
          </div>
          <div class="form-group">
              <label> Adjust time between calls (optional) :</label>
          </div>
          <div class="form-group">
              <label for="test_t1"> Duration of the First Call :</label>
              <input type="input" name="test_t1"  class="form-control" value="5.999999999999999999">
          </div>
          <div class="form-group">
              <label for="test_t2"> Elapsed Time Before Starting the Second Call :</label>
              <input type="input" name="test_t2"  class="form-control" value="2.222222222222222222">
          </div>
          <div class="form-group">
              <label for="test_t3"> Duration of the Second Call :</label>
              <input type="input" name="test_t3"  class="form-control" value="12.22222111111111111">
          </div>

          </div>
          <button type="submit" class="btn btn-primary">Test Call</button>
      </form>
  </div>
</div>
@stop

@section('css')
    
@stop

@section('js')
  
@stop