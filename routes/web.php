<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
//    return view('welcome');
    return view('auth.login');
});


Auth::routes();
/*Route::group(['prefix' => 'ringless'], function () {


    Auth::routes();


});*/
Route::get('/home', 'HomeController@index')->name('home');
/*
Route::get('/test', function () {
        return view('test');
    });
*/
Route::get('/test', 'TestController@index')->name('test');

Route::post('/call','TestController@call');

Route::get('/status', 'TestController@status')->name('status');

Route::get('/execute-status', 'TestCallerController@execute_status');


Route::get('/testcaller', 'TestCallerController@index')->name('test.caller');

Route::post('/calling','TestCallerController@call')->name('testcall');


Route::resource('trunk', 'TrunkController');

Route::resource('country', 'CountryController');

Route::resource('campaign', 'CampaignController');

Route::resource('numbers', 'NumbersController');

Route::resource('run', 'RunController');

Route::post('/run/{run}', ['as' => 'run.update', 'uses' => 'RunController@update']);

Route::get('run-pause/{run}', 'RunController@pause')->name('run.pause');

Route::get('run-start/{run}', 'RunController@start')->name('run.start');

Route::get('ami-channels', 'RunController@ami')->name('run.ami');

#route to download file
Route::get('get-file/{filename}', ['as' => 'numbers.download', 'uses' => 'NumbersController@get_file']);

Route::get('delete-file/{filename}', ['as' => 'numbers.del', 'uses' => 'NumbersController@delete_file']);

Route::get('/changePassword','HomeController@showChangePasswordForm');

Route::post('/changePassword','HomeController@changePassword')->name('changePassword');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('audio-upload', 'AudioUploadController@audioUpload')->name('audio.upload');

//Route::get('audio-list', 'AudioController@index')->name('audio.list');

//Route::get('audio-play/{filename}', 'AudioController@play_audio')->name('audio.play');

//Route::get('audio-delete/{filename}', 'AudioController@delete_file')->name('audio.del');

//Route::post('audio-upload', 'AudioUploadController@audioUploadPost')->name('audio.upload.post');